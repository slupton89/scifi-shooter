﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;

public class AddRoom : ProCamera2DRooms {

    private Camera cam;
    [SerializeField]
    private string RoomID;
    //[SerializeField]
    //private float roomX;
    //[SerializeField]
    //private float roomY;
    //[SerializeField]
    //private float roomWidth;
    //[SerializeField]
    //private float roomHeight;
    [SerializeField]
    private float transitionDuration = .5f;
    [SerializeField]
    private bool scaleToFit;
    [SerializeField]
    private float zoomScale = 1.5f;

    // Use this for initialization
    void Start () {
        cam = FindObjectOfType<Camera>();
        var cameraRooms = cam.GetComponent<ProCamera2DRooms>();
        //cameraRooms.AddRoom(roomX, roomY, roomWidth, roomHeight, .5f, EaseType.Linear, true, false, 1.5f, "");
        var roomRect = transform;
        var newRoom = new Room() {
            ID = RoomID,
            Dimensions = new Rect(transform.position.x, transform.position.z, transform.lossyScale.x, transform.lossyScale.y),
            TransitionDuration = transitionDuration,
            TransitionEaseType = EaseType.Linear,
            ScaleCameraToFit = scaleToFit,
            Zoom = false,
            ZoomScale = zoomScale
        };
        cameraRooms.Rooms.Add(newRoom);
    }

}


